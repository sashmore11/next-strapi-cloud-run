#! /bin/bash

# Activate Google Serice Account responsible for deployments
echo "$GCLOUD_SERVICE_ACCOUNT_KEY" > .service-account-key.json
gcloud auth activate-service-account --key-file .service-account-key.json

# Set te project in gcloud so it knows which project to interact with
gcloud config set project "$GCLOUD_PROJECT_ID"

# Authorize docker so we can push images to Google Container Registry
gcloud auth configure-docker --quiet
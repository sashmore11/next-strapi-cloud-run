#! /bin/bash

apt-get update && apt-get install default-mysql-client -y

query="SELECT count(*) AS TOTALNUMBEROFTABLES FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'production';"
tableCount=$(mysql -u "$DB_USERNAME" -p"$DB_PASSWORD" -h "$DB_HOST" -s -e "$query")

echo ""
echo "Starting database dump..."
if [[ $tableCount = "0" ]]; then
  mysqldump -u "$DB_USERNAME" -p"$DB_PASSWORD" -h "$DB_HOST" "development" > dump.sql
else
  mysqldump -u "$DB_USERNAME" -p"$DB_PASSWORD" -h "$DB_HOST" --no-create-info --replace "development" > dump.sql
fi

echo "Starting database sync..."
mysql -u "$DB_USERNAME" -p"$DB_PASSWORD" -h "$DB_HOST" "production" < dump.sql

echo "Database sync successful!"

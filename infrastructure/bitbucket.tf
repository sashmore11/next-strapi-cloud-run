provider "bitbucket" {
  username = var.bitbucket_username
  password = var.bitbucket_password
}

resource "bitbucket_repository_variable" "gcloud_project_id" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "GCLOUD_PROJECT_ID"
  value      = google_project.project.project_id
  secured    = false
}

resource "bitbucket_repository_variable" "gcloud_service_account_key" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "GCLOUD_SERVICE_ACCOUNT_KEY"
  value      = base64decode(google_service_account_key.bitbucket_pipelines.private_key)
  secured    = true
}

resource "bitbucket_repository_variable" "gcp_region" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "GCP_REGION"
  value      = var.gcp_region
  secured    = false
}

resource "bitbucket_repository_variable" "db_username" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "DB_USERNAME"
  value      = google_sql_user.root.name
  secured    = false
}

resource "bitbucket_repository_variable" "db_password" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "DB_PASSWORD"
  value      = google_sql_user.root.password
  secured    = true
}

resource "bitbucket_repository_variable" "db_name" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "DB_NAME"
  value      = google_sql_database.cms_database_production.name
  secured    = false
}

resource "bitbucket_repository_variable" "db_host" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "DB_HOST"
  value      = google_sql_database_instance.cms_database_instance.ip_address.0.ip_address
  secured    = false
}

resource "bitbucket_repository_variable" "db_socket_path" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "DB_SOCKET_PATH"
  value      = "/cloudsql/${google_sql_database_instance.cms_database_instance.connection_name}"
  secured    = false
}

resource "bitbucket_repository_variable" "cms_graphql_url" {
  repository = "${var.bitbucket_username}/${var.bitbucket_repository_slug}"
  key        = "CMS_GRAPHQL_URL"
  value      = "${google_cloud_run_service.cms.status[0].url}/graphql"
  secured    = false
}
